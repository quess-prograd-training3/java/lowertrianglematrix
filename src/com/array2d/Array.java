package com.array2d;

import java.util.Scanner;

public class Array{
    int[][] array;
    int rows;
    int columns;
    public void initializeArray(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the Rows:- ");
        rows = scanner.nextInt();
        System.out.println("Enter the Columns:- ");
        columns = scanner.nextInt();
        array=new int[rows][columns];
        System.out.println("Enter the Elements:- ");
        for (int iterate = 0; iterate < rows; iterate++) {
            for (int iterate2 = 0; iterate2 < columns; iterate2++) {
                array[iterate][iterate2]=scanner.nextInt();
            }
        }
    }

    public int lowerTriangleMatrix(){
        boolean isLower=false;
        for (int iterate = 0; iterate < rows; iterate++) {
            for (int iterate2 = 0; iterate2 < columns; iterate2++) {
                if (array[iterate][iterate2] != 0) {
                    isLower=false;
                } else {
                    isLower=true;
                }
            }
        }
        if(isLower==true) {
            return 1;
        }
        return 0;
    }
}
